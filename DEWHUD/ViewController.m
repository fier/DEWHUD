//
//  ViewController.m
//  DEWHUD
//
//  Created by Shaw on 2017/12/11.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "ViewController.h"
#import "DEWHUD.h"
#import "UIViewController+DEWCamera.h"
#import "UIViewController+DEWErrorRemind.h"

@interface ViewController ()

@end


@implementation ViewController

//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [DEWHUD showAnimatingWithImages:[self images] oneFrameTime:0.045];
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [DEWHUD hideAnimating];
//        });
//    });
//
//}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor blackColor];
    
    [DEWHUD showRemind:@"暂无数据暂无数据暂无数据暂无数据暂无数据暂无数据暂无数据暂无数据暂无数据暂无数据暂无数据暂无数据暂无数据暂无数据暂无数据暂无数据暂无数据暂无数据暂无数据暂无数据"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [DEWHUD hideRemind];
    });
    
//    UIImageView *imageView = [[UIImageView alloc]init];
//    imageView.backgroundColor = [UIColor redColor];
//    imageView.center = self.view.center;
//    imageView.bounds = CGRectMake(0, 0, 100, 100);
//    [self.view addSubview:imageView];
//
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//
//        [DEWHUD showImagePickerControllerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary AllowEditing:YES cancelAuthRequestAlert:^{
//             NSLog(@"cancel");
//        } result:^(UIImage *originImage, UIImage *editedImage, NSDictionary *mediaInfo) {
//             imageView.image = editedImage;
//        }];
//    });
    

}

//- (NSArray *)images
//{
//
//    NSMutableArray *tempArray = @[].mutableCopy;
//    for (int i = 0; i < 19;  i ++) {
//        NSString *imageName = [NSString stringWithFormat:@"%d",i + 1];
//        UIImage *image = [UIImage imageNamed:imageName];
//        [tempArray addObject:image];
//    }
//    return tempArray.copy;
//}


@end
