//
//  UIViewController+DEWAlert.m
//  DEWHUD
//
//  Created by Shaw on 2017/12/11.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "UIViewController+DEWAlert.h"

@implementation UIViewController (DEWAlert)

- (void)dew_showAlertWithTitle:(NSString *)title
                       message:(NSString *)message
                  actionTitles:(NSArray<NSString *> *)actions
             destructiveTitles:(NSArray <NSString *> *)destructiveActions
               cancelItemTitle:(NSString *)cancelTitle
                    alertStyle:(UIAlertControllerStyle)style
                  actionHandle:(void (^)(NSString *actionTitle))handles
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    
    [actions enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:obj style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (handles) {
                handles(obj);
            }
        }];
        
        [alertController addAction:action];
    }];
    
    [destructiveActions enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:obj style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            if (handles) {
                handles(obj);
            }
        }];
        [alertController addAction:action];
    }];
    
    if (cancelTitle) {
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            if (handles) {
                handles(cancelTitle);
            }
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
        [alertController addAction:cancelAction];
    }
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

@end
