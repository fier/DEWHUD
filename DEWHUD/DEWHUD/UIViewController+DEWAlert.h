//
//  UIViewController+DEWAlert.h
//  DEWHUD
//
//  Created by Shaw on 2017/12/11.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (DEWAlert)

- (void)dew_showAlertWithTitle:(NSString *)title
                       message:(NSString *)message
                  actionTitles:(NSArray<NSString *> *)actions
             destructiveTitles:(NSArray <NSString *> *)destructiveActions
               cancelItemTitle:(NSString *)cancelTitle
                    alertStyle:(UIAlertControllerStyle)style
                  actionHandle:(void (^)(NSString *actionTitle))handles;

@end
