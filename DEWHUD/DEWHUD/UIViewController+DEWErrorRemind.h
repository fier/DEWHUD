//
//  UIViewController+DEWErrorRemind.h
//  DEWHUD
//
//  Created by Shaw on 2017/12/12.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//
//  @abstract 有的页面暂时没有数据，显示一段话提醒一下用户，比如"暂无收藏"

#import <UIKit/UIKit.h>

@interface UIViewController (DEWErrorRemind)

@property (nonatomic, strong) UILabel *dew_remindLabel;

- (void)dew_showErrorRemind:(NSString *)remind;
- (void)dew_showErrorRemind:(NSString *)remind layout:(void(^)(UILabel *remindLabel))layout;

- (void)dew_hideRemind;

@end
