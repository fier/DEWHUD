//
//  UIWindow+DEWPopup.h
//  DEWHUD
//
//  Created by Shaw on 2017/12/11.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (DEWPopup)

///默认居中
- (void)dew_showCustomView:(UIView *)view shouldDisplayBackgroundView:(BOOL)shouldShowBackgroundView;

///自定义布局
- (void)dew_showCustomView:(UIView *)view shouldDisplayBackgroundView:(BOOL)shouldShowBackgroundView layout:(void(^)(UIView *customView, UIWindow *window))layout;

- (void)dew_hideCustomView;

- (void)dew_showAnimatingWithImages:(NSArray <UIImage *> *)images
                       oneFrameTime:(NSTimeInterval)frameRate;

- (void)dew_showAnimatingWithImages:(NSArray <UIImage *> *)images
                       oneFrameTime:(NSTimeInterval)frameRate
                             layout:(void(^)(UIImageView *imageView, UIWindow *window))layout;

- (void)dew_hideAnimating;

@end
