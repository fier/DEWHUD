//
//  DEWHUD.h
//  DEWHUD
//
//  Created by Shaw on 2017/12/11.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DEWHUD : NSObject

+ (void)showToast:(NSString *)toast;
+ (void)showSuccessToast:(NSString *)toast;
+ (void)showFailedToast:(NSString *)toast;

+ (void)showToast:(NSString *)toast completion:(void(^)(void))completion;
+ (void)showSuccessToast:(NSString *)toast completion:(void(^)(void))completion;
+ (void)showFailedToast:(NSString *)toast completion:(void(^)(void))completion;

+ (void)showLoadingToast:(NSString *)toast;

//alert controller
+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
              actionTitles:(NSArray <NSString *> *)actions
         destructiveTitles:(NSArray <NSString *> *)destructiveActions
           cancelItemTitle:(NSString *)cancelTitle
                alertStyle:(UIAlertControllerStyle)style
              actionHandle:(void (^)(NSString *actionTitle))handles;

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
              actionTitles:(NSArray<NSString *> *)actions
           cancelItemTitle:(NSString *)cancelTitle
                alertStyle:(UIAlertControllerStyle)style
              actionHandle:(void (^)(NSString *actionTitle))handles;

//自定义视图
+ (void)showWithCustomView:(UIView *)customView shouldDisplayBackgroundView:(BOOL)shouldShowBackgroundView;
+ (void)showCustomView:(UIView *)view shouldDisplayBackgroundView:(BOOL)shouldShowBackgroundView layout:(void(^)(UIView *customView, UIWindow *window))layout;

//帧动画
+ (void)showAnimatingWithImages:(NSArray *)images oneFrameTime:(NSTimeInterval)frameRate;
+ (void)showAnimatingWithImages:(NSArray *)images oneFrameTime:(NSTimeInterval)frameRate layout:(void(^)(UIImageView *imageView, UIWindow *window))layout;
+ (void)hideAnimating;

// 图片选择器
+ (void)showImagePickerControllerWithSourceType:(UIImagePickerControllerSourceType)sourceType AllowEditing:(BOOL)allow cancelAuthRequestAlert:(void(^)(void))cancelAlert result:(void(^)(UIImage *originImage, UIImage *editedImage,NSDictionary *mediaInfo))result;

// error label

+ (void)showRemind:(NSString *)remind;
+ (void)showRemind:(NSString *)remind layout:(void(^)(UILabel *remindLabel))layout;
+ (void)hideRemind;

@end
