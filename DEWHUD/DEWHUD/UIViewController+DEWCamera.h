//
//  UIViewController+DEWCamera.h
//  DEWHUD
//
//  Created by Shaw on 2017/12/11.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (DEWCamera)

- (void)dew_presentImagePickerControllerWithSourceType:(UIImagePickerControllerSourceType)sourceType AllowEditing:(BOOL)allow cancelAuthRequestAlert:(void(^)(void))cancelAlert result:(void(^)(UIImage *originImage, UIImage *editedImage,NSDictionary *mediaInfo))result;

- (BOOL)dew_canUseCameraWithResult:(void(^)(BOOL canUse))result;

@end
