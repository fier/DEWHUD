
//
//  UIWindow+DEWPopup.m
//  DEWHUD
//
//  Created by Shaw on 2017/12/11.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "UIWindow+DEWPopup.h"

static int kDEWCustomViewTag = 123456789;

@implementation UIWindow (DEWPopup)

- (void)dew_showCustomView:(UIView *)customView shouldDisplayBackgroundView:(BOOL)shouldShowBackgroundView
{
    [self _showCustomView:customView shouldDisplayBackgroundView:shouldShowBackgroundView layout:nil];
}

- (void)dew_showCustomView:(UIView *)view shouldDisplayBackgroundView:(BOOL)shouldShowBackgroundView layout:(void (^)(UIView *, UIWindow *))layout
{
    [self _showCustomView:view shouldDisplayBackgroundView:shouldShowBackgroundView layout:layout];
}

- (void)_showCustomView:(UIView *)customView shouldDisplayBackgroundView:(BOOL)shouldShowBackgroundView layout:(void (^)(UIView *, UIWindow *))layout
{

    [self dew_hideCustomView];
    UIView *backgroudView = [self _backgroundView];
    
    if (shouldShowBackgroundView) {
        backgroudView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.30];
    }else {
        backgroudView.backgroundColor = [UIColor clearColor];
    }

    if (layout) {
        layout(customView,self);
    }else {
        customView.center = self.center;
    }

    [backgroudView addSubview:customView];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(_dewTapSelected:)];
    [backgroudView addGestureRecognizer:tapRecognizer];
    
    [self _animation:^{
        [self addSubview:backgroudView];
    }];
    
}

- (void)dew_hideCustomView
{
    UIView *view = [self viewWithTag:kDEWCustomViewTag];
    [self _animation:^{
        [view removeFromSuperview];
    }];
}

- (void)dew_showAnimatingWithImages:(NSArray *)images oneFrameTime:(NSTimeInterval)frameRate
{
    [self _showAnimatingWithImages:images oneFrameTime:frameRate layout:nil];
}

- (void)dew_showAnimatingWithImages:(NSArray *)images
                       oneFrameTime:(NSTimeInterval)frameRate
                             layout:(void (^)(UIImageView *, UIWindow *))layout
{
    [self _showAnimatingWithImages:images oneFrameTime:frameRate layout:layout];
}

- (void)_showAnimatingWithImages:(NSArray *)images oneFrameTime:(NSTimeInterval)frameRate layout:(void (^)(UIImageView *, UIWindow *))layout
{
    
    if ([images isKindOfClass:[NSArray class]]) {
        if (!images.count) {
            return;
        }
    }
    
    UIImageView *loadingImageView = [[UIImageView alloc]init];
    loadingImageView.animationImages = images;
    loadingImageView.animationDuration = frameRate * images.count;
    loadingImageView.animationRepeatCount = 0;
    [loadingImageView startAnimating];
    
    UIView *backgroundView = [self _backgroundView];
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.000 alpha:0.770];
    [backgroundView addSubview:loadingImageView];
    
    if (layout) {
        layout(loadingImageView, self);
    }else {
        loadingImageView.center = backgroundView.center;
        loadingImageView.bounds = CGRectMake(0, 0, 66, 66);
    }
    
    [self _animation:^{
        [self addSubview:backgroundView];
    }];
    
}

- (void)dew_hideAnimating
{
    [self dew_hideCustomView];
}

#pragma mark - Envet response
- (void)_dewTapSelected:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded) {
        [self _animation:^{
            [sender.view removeFromSuperview];
        }];
    }
}

- (void)_animation:(void(^)(void))animation
{
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        if (animation) {
            animation();
        }
    } completion:nil];
}

#pragma mark - Setters and getters

- (UIView *)_backgroundView
{
    
    UIView *view = [self viewWithTag:kDEWCustomViewTag];
    if (view) return view;
    view = [[UIView alloc]initWithFrame:self.bounds];
    view.tag = kDEWCustomViewTag;
    return view;
}

@end
