//
//  UIViewController+DEWCamera.m
//  DEWHUD
//
//  Created by Shaw on 2017/12/11.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "UIViewController+DEWCamera.h"
#import <AVFoundation/AVFoundation.h>
#import "UIViewController+DEWAlert.h"
#import <objc/runtime.h>


static const char *dew_imagePickerResultBlockKey = "imagePickerResultBlockKey";

typedef void(^DEWImagePickerResultBlock)(UIImage *, UIImage *, NSDictionary *);

@interface UIViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, copy) DEWImagePickerResultBlock resultBlock;

@end


@implementation UIViewController (DEWCamera)

#pragma mark - Public

- (BOOL)dew_canUseCameraWithResult:(void (^)(BOOL))result
{
    
    AVAuthorizationStatus videoAuthStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (videoAuthStatus == AVAuthorizationStatusNotDetermined) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (result) {
                result(granted);
            }
        }];
        return NO;
    }else if (videoAuthStatus == AVAuthorizationStatusRestricted || videoAuthStatus == AVAuthorizationStatusDenied) {
        // 未授权
        if (result) {
            result(NO);
        }
        return NO;
    }else {
        // 已授权
        if (result) {
            result(YES);
        }
        return YES;
    }
}

- (void)dew_presentImagePickerControllerWithSourceType:(UIImagePickerControllerSourceType)sourceType AllowEditing:(BOOL)allow cancelAuthRequestAlert:(void (^)(void))cancelAlert result:(void (^)(UIImage *, UIImage *, NSDictionary *))result
{
    
    [self dew_canUseCameraWithResult:^(BOOL canUse) {
        if (canUse) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.allowsEditing = allow;
            picker.sourceType = sourceType;
            picker.videoQuality = UIImagePickerControllerQualityTypeLow;
            picker.delegate = self;
            self.resultBlock = [result copy];
            
            [self presentViewController:picker animated:YES completion:nil];
        }else {
            
            [self dew_showAlertWithTitle:@"您需要打开相机,相册权限才能继续" message:@"设置-隐私-相机" actionTitles:@[@"确定"] destructiveTitles:nil cancelItemTitle:@"取消" alertStyle:UIAlertControllerStyleAlert actionHandle:^(NSString *actionTitle) {
                if ([actionTitle isEqualToString:@"确定"]) {
                    NSURL * url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                    if([[UIApplication sharedApplication] canOpenURL:url]) {
                        NSURL *settingURL =[NSURL URLWithString:UIApplicationOpenSettingsURLString];
                        
                        if (@available(ios 10.0, *)) {
                            [[UIApplication sharedApplication]openURL:settingURL options:@{} completionHandler:nil];
                        }else {
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated"
                            [[UIApplication sharedApplication] openURL:settingURL];
#pragma clang diagnostic pop
                        }
                    }
                }else {
                    if (cancelAlert) {
                        cancelAlert();
                    }
                }
            }];
        }
    }];
}


#pragma mark - <UIImagePickerControllerDelegate>

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    
    UIImage *editedImage = info[UIImagePickerControllerEditedImage];
    UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
    
    UIImageOrientation originalOrientation = originalImage.imageOrientation;
    if(originalOrientation != UIImageOrientationUp) {
        originalImage = [[UIImage alloc] initWithCGImage:originalImage.CGImage
                                                   scale: 1.0
                                             orientation: UIImageOrientationRight];
    }
    
    UIImageOrientation editedOrientation = editedImage.imageOrientation;
    if (editedOrientation != UIImageOrientationUp) {
        editedImage = [[UIImage alloc] initWithCGImage:editedImage.CGImage
                                                 scale: 1.0
                                           orientation: UIImageOrientationRight];
    }
    
    if (self.resultBlock) {
        self.resultBlock(originalImage, editedImage, info);
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma mark - Privite

- (void)setResultBlock:(DEWImagePickerResultBlock)resultBlock
{
    objc_setAssociatedObject(self, dew_imagePickerResultBlockKey, resultBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (DEWImagePickerResultBlock)resultBlock
{
    return objc_getAssociatedObject(self, dew_imagePickerResultBlockKey);
}


@end
