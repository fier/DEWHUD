//
//  DEWHUD.m
//  DEWHUD
//
//  Created by Shaw on 2017/12/11.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "DEWHUD.h"
#import "UIViewController+DEWAlert.h"
#import <SVProgressHUD.h>
#import "UIWindow+DEWPopup.h"
#import "UIViewController+DEWCamera.h"
#import "UIViewController+DEWErrorRemind.h"

@implementation DEWHUD

typedef enum : NSUInteger {
    DEWHUDToastStyleDefault,
    DEWHUDToastStyleSuccess,
    DEWHUDToastStyleFailed,
} DEWHUDToastStyle;

+ (void)showToast:(NSString *)toast
{
    [self _showToast:toast withStyle:DEWHUDToastStyleDefault completion:nil];
}

+ (void)showToast:(NSString *)toast completion:(void (^)(void))completion
{
    [self _showToast:toast withStyle:DEWHUDToastStyleDefault completion:completion];
}

+ (void)showSuccessToast:(NSString *)toast
{
    [self _showToast:toast withStyle:DEWHUDToastStyleSuccess completion:nil];
}

+ (void)showSuccessToast:(NSString *)toast completion:(void (^)(void))completion
{
    [self _showToast:toast withStyle:DEWHUDToastStyleSuccess completion:completion];
}

+ (void)showFailedToast:(NSString *)toast
{
    [self _showToast:toast withStyle:DEWHUDToastStyleFailed completion:nil];
}

+ (void)showFailedToast:(NSString *)toast completion:(void (^)(void))completion
{
    [self _showToast:toast withStyle:DEWHUDToastStyleFailed completion:completion];
}

+ (void)showLoadingToast:(NSString *)toast
{
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showWithStatus:toast];
}

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
              actionTitles:(NSArray<NSString *> *)actions
         destructiveTitles:(NSArray <NSString *> *)destructiveActions
           cancelItemTitle:(NSString *)cancelTitle
                alertStyle:(UIAlertControllerStyle)style
              actionHandle:(void (^)(NSString *actionTitle))handles
{
    
    [[self getCurrentViewController] dew_showAlertWithTitle:title
                                                    message:message
                                               actionTitles:actions
                                          destructiveTitles:destructiveActions
                                            cancelItemTitle:cancelTitle
                                                 alertStyle:style
                                               actionHandle:handles];
}

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
              actionTitles:(NSArray<NSString *> *)actions
           cancelItemTitle:(NSString *)cancelTitle
                alertStyle:(UIAlertControllerStyle)style
              actionHandle:(void (^)(NSString *actionTitle))handles
{
    
    [[self getCurrentViewController] dew_showAlertWithTitle:title
                                                    message:message
                                               actionTitles:actions
                                          destructiveTitles:nil
                                            cancelItemTitle:cancelTitle
                                                 alertStyle:style
                                               actionHandle:handles];
}

#pragma mark - Custom view

+ (void)showWithCustomView:(UIView *)customView shouldDisplayBackgroundView:(BOOL)shouldShowBackgroundView
{
    [_currentWindow() dew_showCustomView:customView shouldDisplayBackgroundView:shouldShowBackgroundView];
}

+ (void)showCustomView:(UIView *)view shouldDisplayBackgroundView:(BOOL)shouldShowBackgroundView layout:(void (^)(UIView *, UIWindow *))layout
{
    [_currentWindow() dew_showCustomView:view shouldDisplayBackgroundView:shouldShowBackgroundView layout:layout];
}

#pragma mark - Animating

+ (void)showAnimatingWithImages:(NSArray *)images oneFrameTime:(NSTimeInterval)frameRate
{
    [_currentWindow() dew_showAnimatingWithImages:images oneFrameTime:frameRate];
}

+ (void)showAnimatingWithImages:(NSArray *)images oneFrameTime:(NSTimeInterval)frameRate  layout:(void (^)(UIImageView *, UIWindow *))layout
{
    [_currentWindow() dew_showAnimatingWithImages:images oneFrameTime:frameRate layout:layout];
}

+ (void)hideAnimating
{
    [_currentWindow() dew_hideAnimating];    
}

#pragma mark - ImagePicker controller

+ (void)showImagePickerControllerWithSourceType:(UIImagePickerControllerSourceType)sourceType AllowEditing:(BOOL)allow cancelAuthRequestAlert:(void (^)(void))cancelAlert result:(void (^)(UIImage *, UIImage *, NSDictionary *))result
{
    
    [[self getCurrentViewController] dew_presentImagePickerControllerWithSourceType:sourceType AllowEditing:allow cancelAuthRequestAlert:cancelAlert result:result];
}

#pragma mark - Remind label

+ (void)showRemind:(NSString *)remind
{
    
    [[self getCurrentViewController]dew_showErrorRemind:remind];
    
}

+ (void)showRemind:(NSString *)remind layout:(void(^)(UILabel *remindLabel))layout
{
    [[self getCurrentViewController]dew_showErrorRemind:remind layout:layout];
}

+ (void)hideRemind
{
    [[self getCurrentViewController]dew_hideRemind];
}

#pragma mark - Privite

UIWindow *_currentWindow () {
    return [[UIApplication sharedApplication].delegate window];
}

+ (id)getCurrentViewController
{
    UIViewController *WindowRootVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    UIViewController *currentViewController = [self findTopViewController:WindowRootVC];
    return currentViewController;
}

+ (id)findTopViewController:(id)inController
{
    
    if ([inController isKindOfClass:[UITabBarController class]]) {
        return [self findTopViewController:[inController selectedViewController]];
    }else if ([inController isKindOfClass:[UINavigationController class]]) {
        return [self findTopViewController:[inController visibleViewController]];
    }else if ([inController isKindOfClass:[UIViewController class]]) {
        return inController;
    }else {
        return nil;
    }
}

+ (void)_showToast:(NSString *)toast withStyle:(DEWHUDToastStyle)style  completion:(void(^)(void))completion
{
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    
    if (style == DEWHUDToastStyleDefault) {
        [SVProgressHUD showImage:[UIImage imageNamed:@""] status:toast];
    }else if (style == DEWHUDToastStyleSuccess) {
        [SVProgressHUD showSuccessWithStatus:toast];
    }else if (style == DEWHUDToastStyleFailed) {
        [SVProgressHUD showErrorWithStatus:toast];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
        if (completion) {
            completion();
        }
    });
}



@end
