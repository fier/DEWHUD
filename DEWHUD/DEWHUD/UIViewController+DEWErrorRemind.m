//
//  UIViewController+DEWErrorRemind.m
//  DEWHUD
//
//  Created by Shaw on 2017/12/12.
//  Copyright © 2017年 Dawa Co., Ltd. All rights reserved.
//

#import "UIViewController+DEWErrorRemind.h"
#import <objc/runtime.h>

static const char *dew_remindLabelKey = "dew_remindLabelKey";

@implementation UIViewController (DEWErrorRemind)

- (void)dew_showErrorRemind:(NSString *)error
{
   [self _showErrorRemind:error layout:nil];
}

- (void)dew_showErrorRemind:(NSString *)error layout:(void (^)(UILabel *))layout
{
    
    [self _showErrorRemind:error layout:layout];
}

- (void)dew_hideRemind
{
    if (!self.dew_remindLabel) {
        return;
    }
    
    [self.dew_remindLabel removeFromSuperview];
}

#pragma mark - Privite

- (void)_showErrorRemind:(NSString *)error layout:(void (^)(UILabel *))layout
{
    
    if (!self.dew_remindLabel) {
        self.dew_remindLabel = [[UILabel alloc]init];
        self.dew_remindLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:18];
        self.dew_remindLabel.textColor = [UIColor lightTextColor];
        self.dew_remindLabel.textAlignment = NSTextAlignmentCenter;
        self.dew_remindLabel.numberOfLines = 0;
    }
    
    self.dew_remindLabel.text = error;
    [self.view addSubview:self.dew_remindLabel];
    
    if (layout) {
        layout(self.dew_remindLabel);
    }else {
        self.dew_remindLabel.frame = CGRectMake(20, 0, self.view.bounds.size.width - 40, self.view.bounds.size.height);
    }
}

#pragma mark - Setters and getters

- (void)setDew_remindLabel:(UILabel *)dew_remindLabel
{
    objc_setAssociatedObject(self, dew_remindLabelKey, dew_remindLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UILabel *)dew_remindLabel
{
     return objc_getAssociatedObject(self, dew_remindLabelKey);
}

@end
